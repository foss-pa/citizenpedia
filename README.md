# Citizenpedia

La piattaforma è composta da due moduli software interdipendenti:

1. [Collaborative Procedure Designer](https://bitbucket.org/foss-pa/cpd) (CPD)
2. [Question and Answer Engine](https://bitbucket.org/foss-pa/qae) (QAE)

I repository di entrambi i moduli software si trovano accanto al presente repository e sono raggiungibili dai due link sopra.

---

*Per l'installazione di ciascun modulo, siete pregati di fare riferimento alla rispettiva documentazione.*
